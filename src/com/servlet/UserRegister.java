package com.servlet;

import com.dao.LoginDaolmp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
//

/**
 * Servlet implementation class UserRegister
 */
@WebServlet("/UserRegister")
public class UserRegister extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String username = request.getParameter("zhucename");//
        String userpwd = request.getParameter("zhucepwd");//
        String telephone = request.getParameter("telephone");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        LoginDaolmp zhuce = new LoginDaolmp();//实例化对象
        boolean isHave = zhuce.RegisterName(username, userpwd, name, telephone, email);
        if (isHave) {//如果有的话
            request.setAttribute("username", username);//显示注册成功然后跳到登录界面
            System.out.println("注册成功！");
            request.getRequestDispatcher("index.jsp").forward(request, response);//返回到index

        } else {//
            request.getSession().setAttribute("info", "account does not right");
            //      request.setAttribute("successzhuce","注册成功");
            response.sendRedirect("register.jsp");
        }
    }
}
