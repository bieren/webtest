package com.dao;

import com.bean.User;
import com.util.DButils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LoginDaolmp {
    //登录时查询用户名和密码是否匹配，如果匹配则返回真
    public boolean searchName(String loginName, String loginpwd) {//数据库查找有没有这个人
        Connection conn = (Connection) DButils.getConnection();
        String sql = "select * from user where username=? and userpwd=?";//已经限定了某一行 要么有要么没有
        try {
            PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);//执行sql语句
            ps.setString(1, loginName);//赋值给loginname
            ps.setString(2, loginpwd);
            ResultSet rs = ps.executeQuery();//如果不存在
            while (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return false;
    }

    //显示学生信息
    public static List<User> fhui(String loginName, String loginpwd) {//根据pwd来返回信息
        Connection conn = (Connection) DButils.getConnection();
        String sql = "select * from user where username=? and userpwd=?";//已经限定了某一行 要么有要么没有
        List<User> list=new ArrayList<>();
        String a[] = new String[6];
        try {
            PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);//执行sql语句
            ps.setString(1, loginName);//赋值给loginname
            ps.setString(2, loginpwd);
            ResultSet rs = ps.executeQuery();//如果不存在
            if (rs.next() == true) {
                User user=new User();
                user.setUsername(rs.getString(1));
                user.setUserpwd(rs.getString(2));
                user.setName(rs.getString(3));
                user.setTelephone(rs.getString(4));
                user.setEmail(rs.getString(5));
                user.setSex(rs.getString(6));
                list.add(user);
            }
        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return list;
    }


    //注册时将新用户名和信息插入数据库，操作成功则返回真
    public boolean RegisterName(String loginName, String loginpwd, String name, String rtelephone, String remail) {//将数据插入数据库
        Connection conn = (Connection) DButils.getConnection();
        PreparedStatement ps = null;
        String sql = "insert into user(username,userpwd,name,telephone,email,sex) values(?,?,?,?,?,?)";//将注册获取的信息插入到数据库里
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, loginName);
            ps.setString(2, loginpwd);
            ps.setString(3, name);
            ps.setString(4, rtelephone);
            ps.setString(5, remail);
            ps.setString(6,"男");
            int result = ps.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return false;
    }

}
