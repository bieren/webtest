<%--
  Created by IntelliJ IDEA.
  User: lsy
  Date: 2023/5/7
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%--<%@page isELIgnored="false" %>--%>
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">--%>
<html>
<head>
    <title>聊天界面</title>
    <link type="text/css" rel="stylesheet" href="/css/chat.css">
    <link rel="shortcut icon" href="#"/>
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        //加载后立即连接
        var ws;
        var ws_url = "ws://localhost:8080/websocket";
        var lockReconnect=false;
        $(function (){
            ws_connect();
            $(".btn").click(function (){
                ws_sendMsg();
            });
        });

        // function reconnect() {
        //     if(lockReconnect) return;
        //     lockReconnect = true;
        //     setTimeout(function () {     //没连接上会一直重连，设置延迟避免请求过多
        //         ws_connect();
        //         lockReconnect = false;
        //     }, 2000);
        // }
        window.onbeforeunload = function() {
            ws.close();
        }
        function ws_connect() {
            var userinf='${sessionScope.name}';
            if ('WebSocket' in window) {
                console.log('111');
                ws = new WebSocket(ws_url+"?user="+userinf);
            } else if ('MozWebSocket' in window) {
                ws = new MozWebSocket(ws_url);
            } else {
                console.log('Error: WebSocket is not supported by this browser.');
                return;
            }
            ws.onopen = function () {
                console.log('Info: WebSocket connection opened.');
            };
            ws.onclose = function (e) {
                console.log('websocket 断开: ' + e.code + ' ' + e.reason + ' ' + e.wasClean)
                console.log(e);
                console.log('Info: WebSocket closed.');
            };
            ws.onmessage = function (message) {//后端传的messag
                 console.log(message.data);
                 var str2=message.data;//后面带user
                 var time = new Date().toLocaleTimeString();//实时获取时间
                 const array=str2.split("user=");
                 str2=array[0];
                 var user=array[1];
              if(str2!="") {
                  $(".chat-messages").append("<div class='contentleft'><div class='yuanleft'>"+user+ "</div><div class='left2'><div>" + time + "</div><div class='leftzi'>" + str2 + "</div></div></div>")
              }
            };
        }
        // 聊天对象
        function ws_sendMsg() {
            var chatInput = document.querySelector('.chat-input input[type="text"]');
            var  msg=chatInput.value;//要发的信息
            ws.send(msg);
            document.querySelector('.chat-input input[type="text"]').value='';
            console.log("发送成功")
        }
    </script>
</head>
<body>
<%--        欢迎${sessionScope.username}来到系统--%>
        <div class="chatcontain">
            <div class="chat-header">
                <h2>Chat</h2>
            </div>
            <div class="chat-messages">
                <!-- 信息面板 -->
            </div>
            <div class="chat-input">
                <form>
                    <input type="text" placeholder="请输入消息">
                    <input type="button" class="btn" value="发送">
                    <input type="text"style="display: none"/>
                </form>

            </div>
        </div>
</body>
</html>