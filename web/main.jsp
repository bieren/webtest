<%@ page import="com.bean.User" %>
<%@ page import="com.dao.LoginDaolmp" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/5/6
  Time: 14:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
    <style>
        /*兼容浏览器*/
        * {
            margin: 0;
            padding: 0;
        }

        .head {
            height: 90px;
            background-color: darkcyan;
            font-size: 20px;
            font-weight: 550;
            line-height: 80px;
            color: white;
            padding-left: 90px;
        }

        .content {
            width: 100%;
            height: 100%;
        }

        .content-left {
            width: 15%;
            text-align: center;
            height: 700px;
            background-color: #1c232f;
            float: left;
        }

        .content-right {
            width: 85%;
            height: 700px;
            float: left;
        }

        .left-title {
            width: 100%;
            height: 88px;
            padding-top: 45px;
        }

        .left-title a {
            display: block;
            width: 100%;
            height: 50px;
            line-height: 50px;
            text-align: center;
            color: white;
            /*去掉a下默认下划线*/
            text-decoration: none;
        }

        /*分割线*/
        .seg {
            height: 1px;
            width: 100%;
            background-color: black;
        }

        .nav {
            /*上下5,左右0*/
            margin: 5px 0;
            height: 105px;
            padding-top: 20px;
        }

        /*菜单项主标题*/
        .nav-title {
            height: 80px;
            width: 100%;
            color: white;
            text-align: center;
            line-height: 80px;
            cursor: pointer;
        }

        /*子标题内容区*/
        .nav-content {
            width: 100%;
            height: 50%;
            background-color: #0C1119;
            padding-top: 17px;
        }

        /*子标题的样式*/
        .nav-content a {
            display: block;
            width: 100%;
            height: 30px;
            color: #CCCCCC;
            text-decoration: none;
            text-align: center;
            line-height: 30px;
            font-size: 13px;
        }

        /*子标题鼠标经过时的改变颜色*/
        .nav-content a:hover {
            color: #FFFFFF;
            background-color: #12040c;
        }

        /*内容区*/
        .content-right {
            font-size: 50px;
            line-height: 600px;
            text-align: center;
        }

        #menuFrame {
            border-top: none;
            border-left: none;
            box-sizing: border-box;
        }

        .headright {
            color: white;
            font-size: 17px;
            font-weight: 300;
            float: right;
            padding-right: 100px;
        }

        .back {
            width: 65px;
            color: black;
            background-color: darkcyan;
            font-size: 17px;
            margin-left: 45px;
            border: 1px solid darkcyan;
            box-shadow: 0 0 5px rgba(0.1, 0.1, 0.1, 0.35);
        }

        .back:hover {
            color: #C4FCEF;
        }

        .foot {
            width: 100%;
            height: 50px;
            background-color: #E6E6FA;
            clear: both;
            text-align: center;
            line-height: 50px;
        }
    </style>

    <script>

        window.onload = function () {
            //隐藏所有子标题

            var navMenus = document.querySelectorAll('.nav-menu');
            navMenus.forEach(function (navMenu) {
                var navContent = navMenu.querySelector('.nav-content');
                navContent.style.display = 'none';
            });
            //给菜单项中的所有主标题绑定事件
            var navTitles = document.querySelectorAll('.nav-title');
            navTitles.forEach(function (navTitle) {
                //获取点击当前标签下所有子标签
                var navConEle = navTitle.parentNode.querySelector('.nav-content');
                //绑定点击事件
                navTitle.addEventListener('click', function () {
                    if (navConEle.style.display != 'none') {
                        //隐藏
                        navConEle.style.display = 'none';
                    } else {
                        //显示
                        navMenus.forEach(function (navMenu) {//forEach 来遍历子navmenu
                            var navContent = navMenu.querySelector('.nav-content');
                            navContent.style.display = 'none';
                        });
                        navConEle.style.display = 'block';
                    }
                });
            });

        };
    </script>
</head>
<body>
<div class="head">
    聊天室系统
    <div class="headright">
        当前登录的用户是:${sessionScope.name}
        <button class="back" onclick="window.location.href='index.jsp'">退&nbsp;出</button>
    </div>
</div>

<div class="content">
    <!--左侧导航栏-->
    <div class="content-left">
        <div class="left-title">
            <a href="transform.jsp" target="menuFrame">Weclome</a>
        </div>
        <div class="seg"></div>
        <!--菜单栏导航-->
        <div class="nav">
            <div class="nav-menu">
                <div class="nav-title">聊天室管理</div>
                <div class="nav-content">
                    <a href="chat.jsp" target="menuFrame">群聊聊天室</a>
                </div>
            </div>
        </div>
        <div class="seg"></div>
        <div class="nav">
            <div class="nav-menu">
                <div class="nav-title">信息管理</div>
                <div class="nav-content">
                    <a href="inform.jsp" target="menuFrame">我的信息</a>
                </div>
            </div>
        </div>
        <div class="seg"></div>
        <div class="nav">
            <div class="nav-menu">
                <div class="nav-title">其它管理</div>
                <div class="nav-content">
                    <a href="#">其它查看</a>
                    <a href="#">其它删除</a>
                </div>
            </div>
        </div>

        <div class="seg"></div>
    </div>
    <div class="content-right">
        <iframe id="menuFrame" name="menuFrame" src="transform.jsp" style=" width: 100%;
                height: 100%;
                /*background-color: #6495ED;*/
                float: left;"></iframe>
    </div>
    <div class="foot">
        Copyright 2023 ©lsy
    </div>
</div>
</body>
</html>