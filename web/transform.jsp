<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/5/13
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<style>
  *{
    padding:0;
    margin: 0;
  }
  body{
    display: flex;
    background: linear-gradient(125deg,#C4FCEF,#2980b0,#28a8a3,lightsteelblue,#27ae60);
    /* 旋转一定角度 */
    animation: move 20s linear infinite;
    /* 动画名*/
    /* 线性过渡*/            /*   无限次数 */
    background-size: 500%;

  }
  .text{
    color:white;
    font-size: 25px;
    font-weight: 550;
    text-align: center;
    position: absolute;
    top: 320px;
    left:530px;
  }
  /* 格式*/
  @keyframes move {
    0%{
      background-position: 0% 50%;
    }
    50%{
      background-position: 100% 50%;
    }
    100%{
      background-position: 0% 50%;
    }
  }
</style>
<body>
<div class="head">
</div>
<div class="text">
  Welcome to this system
</div>
</body>
</html>
