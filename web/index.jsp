<%--
  Created by IntelliJ IDEA.
  User: lsy
  Date: 2023/4/23
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>登录/注册</title>
    <link type="text/css" rel="stylesheet" href="/css/login.css">
    <link rel="shortcut icon" href="#"/>
</head>
<body>

<form action="loginServlet" method="post" id="form">
    <div class="container">
        <div class="head">
            <div class="leftzi"><a href="#" class="leftzi">首页</a></div>
            <div class="mid">没账号？</div>
            <div class="rightzi"><a href="register.jsp" class="rightzi">
                去注册
            </a></div>
        </div>
        <div class="login">Login</div>
        <div class="inputtop">
            <label>用户名</label>
            <input type="text" size="25" class="beau" id="username" name="username" placeholder="请输入用户名" required>
        </div>
        <div class="inputbuttom">
            <label>密&nbsp&nbsp&nbsp码</label>
            <input type="password" id="userpwd" name="userpwd" size="25" class="beau" placeholder="请输入密码" required>
            <%--    设置id属性在servlet层要调用          --%>
        </div>
        <div class="sub">
            <input type="submit" class="dl" value="登录">
        </div>
        <c:if test="${!empty errormsg}">
            <div class="loginerror">
                    ${errormsg}
            </div>
        </c:if>
    </div>
</form>
</body>
</html>
