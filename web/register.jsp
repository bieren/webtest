<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/4/23
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>注册界面</title>
    <link type="text/css" rel="stylesheet" href="/css/register.css">
    <link rel="shortcut icon" href="#"/>
</head>
<body>
<div class="qudl">
    <a href="index.jsp">去登录</a>
</div>
<form action="UserRegister" method="post" id="form2">
    <h1>注册新账号</h1>
    <label for="zhucename">账号</label>
    <input type="text" name="zhucename" id="zhucename" required>
    <label for="email">邮箱</label>
    <input type="email" name="email" id="email" required>
    <label for="name">姓名</label>
    <input type="text" name="name" id="name" required>

    <label for="zhucepwd">密码</label>
    <%--       label for 显示绑定注意 input的id和labelforid 对应--%>
    <input type="password" name="zhucepwd" id="zhucepwd" required>

    <label for="telephone">手机号</label>
    <input type="text" name="telephone" id="telephone" required>

    <input type="submit" value="注册" class="resgi">
    <input type="reset" value="取消" class="clear">
</form>

</body>
<script>

</script>
</html>
