<%@ page import="com.bean.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/5/13
  Time: 12:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="com.bean.User" %>
<%@ page import="com.dao.LoginDaolmp" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<html>
<head>
    <title>Title</title>
    <style>
        body {
            background-image: url(https://pic3.zhimg.com/80/v2-3b83e03ec8352b504e7a3dab903a9c66_1440w.webp);
            background-size: cover;
        }

        .head {
            width: 1260px;
            height: 130px;

            position: relative;
        }

        .headimg {
            width: 90px;
            height: 90px;
            border-radius: 50%;
            background-image: url(images/img_1.png);
            background-size: cover;
            position: absolute;
            top: 25px;
            left: 100px;
        }

        .iname {
            width: 100px;
            height: 50px;
            position: absolute;
            top: 120px;
            left: 130px;
        }

        .infm {
            width: 150px;
            height: 70px;
            position: absolute;
            top: 45px;
            left: 270px;
            font-size: 17px;
            font-weight: 300;
        }

        .fens {

            width: 60px;
            height: 45px;
            position: absolute;
            top: 65px;
            right: 285px;
            border-right: 1px dashed;
            text-align: center;
        }

        .focus {
            width: 60px;
            height: 45px;
            position: absolute;
            top: 65px;
            right: 210px;
            border-right: 1px dashed;
            text-align: center;
        }

        .dianz {
            width: 60px;
            height: 45px;
            position: absolute;
            top: 65px;
            right: 130px;
            border-right: 1px dashed;
            text-align: center;
        }

        .tinform {
            width: 660px;
            height: 450px;
            opacity: 0.7;
            border: 1px solid white;
            position: absolute;
            top: 170px;
            left: 300px;
            box-shadow: 0px 0px 1.5px 1.5px silver;
            border-radius: 3px;
        }

        .sk {
            width: 400px;
            height: 50px;
            margin-left: 170px;
            margin-top: 30px;
            color: white;
            font-weight: bold;
        }

        .ger {
            margin-left: 200px;
            color: aliceblue;
            font-size: 20px;
        }

        .back {
            position: absolute;
            right: 0px;
            top: 10px;
            width: 45px;
            height: 25px;
            border: 1px solid lightblue;
            border-radius: 3px;
            text-align: center;
            line-height: 25px;
        }

        .back a:hover {
            color: lightgray;
        }

        .back a {
            text-decoration: none;
            color: white;
        }
    </style>
</head>
<body>
<%
    String p1 = (String) session.getAttribute("username");
    String p2 = (String) session.getAttribute("userpwd");
    List<User> list = LoginDaolmp.fhui(p1, p2);
    Iterator<User> iter = list.iterator();
    User u=iter.next();


%>
<div class="head">
    <div class="headimg"></div>
    <div class="iname"><%=u.getName()%></div>
    <div class="infm">账号：&nbsp;<%=u.getUsername()%></div>
    <div class="fens">3</br>粉丝</div>
    <div class="focus">5</br>关注</div>
    <div class="dianz">6</br>点赞</div>
    <div class="back"><a href="#">点赞</a></div>
</div>
<div class="ger"> 个人信息:</div>
<div class="tinform">
    <div class="sk">姓名:&nbsp;&nbsp;<%=u.getName()%></div>
    <div class="sk">手机号:&nbsp;<%=u.getTelephone()%>&nbsp;</div>
    <div class="sk">电子邮箱:&nbsp;&nbsp;<%=u.getEmail()%></div>
    <div class="sk">性别:&nbsp;&nbsp;&nbsp;&nbsp;男</div>
    <div class="sk">职位:&nbsp;&nbsp;&nbsp;&nbsp;学生</div>

</div>

</body>
</html>
